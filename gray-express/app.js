var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
var router = express.Router();

app.disable("x-powered-by");

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

const env = app.get("env");
if (env === "production") {
    // use things specefic for production env
    app.use(express.static(path.join(__dirname, "/../gray-vue/dist")));
} else if (env === "development") {
    // dev mode. remember to build your front end project to serve it as SPA
    app.use(express.static(path.join(__dirname, "/../gray-vue/dist")));
}

// API routes 
app.use('/api/users', usersRouter);

//catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404)); // get here when no routes is working
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // redirect to error page ?
    res.status(err.status || 500);
    res.json({
        error: {},
        message: err.message,
    });
});

module.exports = app;