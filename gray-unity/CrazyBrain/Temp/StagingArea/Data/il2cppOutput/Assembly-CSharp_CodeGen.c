﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void example::Start()
extern void example_Start_m05815A251E643D4B4BC76DB1CED1B200A5A60395 ();
// 0x00000002 System.Void example::Update()
extern void example_Update_m624681DA73DDA3F8A21F8306E15222F3FE425E33 ();
// 0x00000003 System.Void example::OnMouseDown()
extern void example_OnMouseDown_mA64DE85C931DEE92E7F9AA719758DEBFEB8B20F3 ();
// 0x00000004 System.Void example::StartPaint()
extern void example_StartPaint_mC9EC4E758F8DC54B506AADD5406789CEB4598B76 ();
// 0x00000005 System.Void example::StopPaint()
extern void example_StopPaint_m9B3181E4F66F2170400BC7C89501B6ACFA9FEFA1 ();
// 0x00000006 System.Void example::.ctor()
extern void example__ctor_m061CA5C7E7DCE8D2A57190541EF43DAB50E9059B ();
// 0x00000007 System.Void ColorPickerTriangle::Awake()
extern void ColorPickerTriangle_Awake_mE258F4FDDAC9880513DD2A94B1C6C2857DA59245 ();
// 0x00000008 System.Void ColorPickerTriangle::Update()
extern void ColorPickerTriangle_Update_m7E49861CBD6E3FCA054761836879412A6C92FD94 ();
// 0x00000009 System.Void ColorPickerTriangle::StopDrag()
extern void ColorPickerTriangle_StopDrag_mC332FB6B0DC1A546157A03632CACC5CDF5AE6E2C ();
// 0x0000000A System.Boolean ColorPickerTriangle::HasIntersection()
extern void ColorPickerTriangle_HasIntersection_m7D2F15C70B5C3D3E2B945279AFE235436AA82A2B ();
// 0x0000000B System.Void ColorPickerTriangle::SetNewColor(UnityEngine.Color)
extern void ColorPickerTriangle_SetNewColor_mB53880FE9470E7C01791CB4096F86F32F2EE2B04 ();
// 0x0000000C System.Void ColorPickerTriangle::CheckCirclePosition()
extern void ColorPickerTriangle_CheckCirclePosition_m75130B33D7A56F63EC2FDE9388A9FD4155C162C5 ();
// 0x0000000D System.Void ColorPickerTriangle::CheckTrianglePosition()
extern void ColorPickerTriangle_CheckTrianglePosition_mE0B667A7ED7F29792ECA0DEAE851A4426BCE37E2 ();
// 0x0000000E System.Void ColorPickerTriangle::SetColor()
extern void ColorPickerTriangle_SetColor_mC2839F3440B673E9F891F6A6CB4D71533D10F654 ();
// 0x0000000F System.Void ColorPickerTriangle::ChangeTriangleColor(UnityEngine.Color)
extern void ColorPickerTriangle_ChangeTriangleColor_m49CCDC38D8AF3BE56693E103C9E4835FFB10BB0B ();
// 0x00000010 UnityEngine.Vector3 ColorPickerTriangle::Barycentric(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ColorPickerTriangle_Barycentric_m52C0218214AEFB9D4FF7DA769951B8F21A9704C0 ();
// 0x00000011 System.Void ColorPickerTriangle::SetTrianglePoints()
extern void ColorPickerTriangle_SetTrianglePoints_mF112985D35C2B215F0FA7F1A5042626F3C3885BD ();
// 0x00000012 System.Void ColorPickerTriangle::.ctor()
extern void ColorPickerTriangle__ctor_m94E1CCBDFC045B4F5E454A45CABC5D9B81DFF946 ();
// 0x00000013 System.Void BrainControl::RotateRight(System.Single)
extern void BrainControl_RotateRight_mC8A0856681AF174FFFDE1E39A7D13D888AF818F3 ();
// 0x00000014 System.Void BrainControl::RotateLeft(System.Single)
extern void BrainControl_RotateLeft_m0AF0CF451D40DC1E176935FAB659936B75B8E571 ();
// 0x00000015 System.Void BrainControl::RotateUp(System.Single)
extern void BrainControl_RotateUp_m04D4A36B5646E9CBE79CBCDD3C57AE4B2C89412D ();
// 0x00000016 System.Void BrainControl::RotateDown(System.Single)
extern void BrainControl_RotateDown_mA8CC9D28CA1AC8A019CBA768ECFC1C8138CCEEEA ();
// 0x00000017 System.Void BrainControl::SizeUp()
extern void BrainControl_SizeUp_m0CE1EAFE75EC2FB7F6ABCD0E0788A1167F885DAC ();
// 0x00000018 System.Void BrainControl::SizeDown()
extern void BrainControl_SizeDown_m9A1432E67ED0BE65EB81687E583959C39ADF0E7B ();
// 0x00000019 System.Void BrainControl::.ctor()
extern void BrainControl__ctor_m43645618B304085EA94C1328EB41A9987B543614 ();
static Il2CppMethodPointer s_methodPointers[25] = 
{
	example_Start_m05815A251E643D4B4BC76DB1CED1B200A5A60395,
	example_Update_m624681DA73DDA3F8A21F8306E15222F3FE425E33,
	example_OnMouseDown_mA64DE85C931DEE92E7F9AA719758DEBFEB8B20F3,
	example_StartPaint_mC9EC4E758F8DC54B506AADD5406789CEB4598B76,
	example_StopPaint_m9B3181E4F66F2170400BC7C89501B6ACFA9FEFA1,
	example__ctor_m061CA5C7E7DCE8D2A57190541EF43DAB50E9059B,
	ColorPickerTriangle_Awake_mE258F4FDDAC9880513DD2A94B1C6C2857DA59245,
	ColorPickerTriangle_Update_m7E49861CBD6E3FCA054761836879412A6C92FD94,
	ColorPickerTriangle_StopDrag_mC332FB6B0DC1A546157A03632CACC5CDF5AE6E2C,
	ColorPickerTriangle_HasIntersection_m7D2F15C70B5C3D3E2B945279AFE235436AA82A2B,
	ColorPickerTriangle_SetNewColor_mB53880FE9470E7C01791CB4096F86F32F2EE2B04,
	ColorPickerTriangle_CheckCirclePosition_m75130B33D7A56F63EC2FDE9388A9FD4155C162C5,
	ColorPickerTriangle_CheckTrianglePosition_mE0B667A7ED7F29792ECA0DEAE851A4426BCE37E2,
	ColorPickerTriangle_SetColor_mC2839F3440B673E9F891F6A6CB4D71533D10F654,
	ColorPickerTriangle_ChangeTriangleColor_m49CCDC38D8AF3BE56693E103C9E4835FFB10BB0B,
	ColorPickerTriangle_Barycentric_m52C0218214AEFB9D4FF7DA769951B8F21A9704C0,
	ColorPickerTriangle_SetTrianglePoints_mF112985D35C2B215F0FA7F1A5042626F3C3885BD,
	ColorPickerTriangle__ctor_m94E1CCBDFC045B4F5E454A45CABC5D9B81DFF946,
	BrainControl_RotateRight_mC8A0856681AF174FFFDE1E39A7D13D888AF818F3,
	BrainControl_RotateLeft_m0AF0CF451D40DC1E176935FAB659936B75B8E571,
	BrainControl_RotateUp_m04D4A36B5646E9CBE79CBCDD3C57AE4B2C89412D,
	BrainControl_RotateDown_mA8CC9D28CA1AC8A019CBA768ECFC1C8138CCEEEA,
	BrainControl_SizeUp_m0CE1EAFE75EC2FB7F6ABCD0E0788A1167F885DAC,
	BrainControl_SizeDown_m9A1432E67ED0BE65EB81687E583959C39ADF0E7B,
	BrainControl__ctor_m43645618B304085EA94C1328EB41A9987B543614,
};
static const int32_t s_InvokerIndices[25] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	1117,
	23,
	23,
	23,
	1117,
	1545,
	23,
	23,
	289,
	289,
	289,
	289,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	25,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
