﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainControl : MonoBehaviour
{

    public GameObject brain;

    public void RotateRight()
    {
        brain.transform.Rotate(0, brain.transform.rotation.y + 10, 0);
    }
    public void RotateLeft()
    {
        brain.transform.Rotate(0, brain.transform.rotation.y - 10, 0);
    }
    public void RotateUp()
    {
        brain.transform.Rotate(brain.transform.rotation.x - 10, 0, 0);
    }
    public void RotateDown()
    {
        brain.transform.Rotate(brain.transform.rotation.x + 10, 0, 0);
    }
    public void SizeUp()
    {
        brain.transform.localScale = brain.transform.localScale + brain.transform.localScale * 0.2f;
    }
    public void SizeDown()
    {
        brain.transform.localScale = brain.transform.localScale + brain.transform.localScale * -0.2f;
    }
}
