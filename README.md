*Minimal Project
Vue.js Express.js Unity*

- gray-express
- gray-vue
- gray-unity

Project Link https://bitbucket.org/tawfiknasser/minimal-vue-express-unity

___
## gray-unity
![](https://i.imgur.com/NZJsr1S.png)

![](https://i.imgur.com/kLf9bnJ.png)

CrazyBrainBuild is built version for WebGl
You can run it on your browser by simply openning the *index.html* file.
CrazyBrain folder is the unity project.

Simple control script :
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainControl : MonoBehaviour
{

    public GameObject brain;

    public void RotateRight()
    {
        brain.transform.Rotate(0, brain.transform.rotation.y + 10, 0);
    }
    public void RotateLeft()
    {
        brain.transform.Rotate(0, brain.transform.rotation.y - 10, 0);
    }
    public void RotateUp()
    {
        brain.transform.Rotate(brain.transform.rotation.x - 10, 0, 0);
    }
    public void RotateDown()
    {
        brain.transform.Rotate(brain.transform.rotation.x + 10, 0, 0);
    }
    public void SizeUp()
    {
        brain.transform.localScale = brain.transform.localScale + brain.transform.localScale * 0.2f;
    }
    public void SizeDown()
    {
        brain.transform.localScale = brain.transform.localScale + brain.transform.localScale * -0.2f;
    }
}
```
___
## gray-vue
Simple vue project that serve the webGL build.
>First *npm i*
Then *npm run serve*.
*npm run build* if you want to build for production or to be served by the express server below.

Sending messages from Vue component to Your Unity instance : 

add ref 
```
  <unity 
  src="CrazyBrainBuild/Build/CrazyBrainBuild.json" 
  width="1000"
  height="600" 
  unityLoader="CrazyBrainBuild/Build/UnityLoader.js"
  ref="instance"></unity>  
```
then call this ref inside your component methods with message method
```
  methods: {
    LeftClick() {
      this.$refs.instance.message("Controller", "RotateLeft");
    }
  }
```
*Ref :* https://docs.unity3d.com/Manual/webgl-interactingwithbrowserscripting.html

 
###### *CrazyBrainBuild files is copied into gray-vue project, inside the public folder.
###### If you don't want to duplicate your code, and serve these file from gray-unity project,
###### You will need to modify your bundler configurations.


___
## gray-express
Simple express project that serve vue SPA
> *npm i*
*npm start*

How express router would look like : 

![](https://i.imgur.com/olNCsBa.png)
*Ref :* https://github.com/foundersandcoders/open-tourism-platform



